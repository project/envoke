<?php

namespace Drupal\envoke;

use \Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Envoke Service.
 *
 */
class EnvokeService {

  const SLEEP_INCREMENT = 10000;
  const SLEEP_LIMIT = 60000;

  /**
   * The Envoke config settings
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Guzzle HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *    The HTTP client.
   */
  public function __construct($config, ClientInterface $http_client) {
    $this->config = $config;
    $this->httpClient = $http_client;
  }

  /**
   * Return boolean to indicate sending email is successful.
   *
   * @param string $to receiver email address
   * @param array $message array of data fields required by Envoke API
   * @return bool
   *
   * TODO: Update this function to remove the $to param since it is passed with
   *         $message.
   */
  public function sendEmail($to, $message)
  {
    $username = $this->config->get('envoke_api_id');
    $password = $this->config->get('envoke_api_key');
    $envokeSendMailApiUrl = 'https://e1.envoke.com/api/v4legacy/send/SendEmails';

    if (empty($username) || empty($password)) {
      return FALSE;
    }

    $data = [
      "SendEmails" => [
        [
          "EmailDataArray" => [
            [
              "email" => [$message]
            ]
          ]
        ]
      ]
    ];

    $sleepTime = 0;
    while (!$contactExists = $this->insertContactIfNotExist($to)) {
      $sleepTime += self::SLEEP_INCREMENT;
      if ($sleepTime > self::SLEEP_LIMIT) {
        break;
      }
      usleep($sleepTime);
    }

    if ($contactExists) {
      try {
        $sendMailRequestResponse = $this->httpClient->post($envokeSendMailApiUrl, [
          'auth' => [$username, $password],
          'json' => $data,
        ]);
        $result = json_decode($sendMailRequestResponse->getBody(), true);
        return $result[0]['result_value'] == "true";
      } catch (RequestException $exception) {
        // TODO: Add drupal watchdog
        // Logs an error
        \Drupal::logger('envoke')->error(
          'The Envoke API to send an email to !email returned an exception and the email could not be sent.',
          ['!email' => $to]
        );
        $logger = \Drupal::logger('envoke');
        Error::logException($logger, $exception);
        return FALSE;
      }
    } else {
      // Logs an error
      \Drupal::logger('envoke')->error(
        'Could not use the Envoke API to send an email to !email because the contact could not be created / verified.',
        ['!email' => $to]
      );
      return FALSE;
    }
  }

  /**
   * Return boolean to indicate the insertion of a new contact with the given email is successful
   * @param string $email
   * @param array $subscribed array of subscriptions (e.g Newsletter, ...)
   * @param array $unsubscribed array of subscriptions (e.g Newsletter, ...)
   * @param bool $forSubscription to update subscriptions
   * @return bool
   */
  public function insertContactIfNotExist($email, $subscribed = [], $unsubscribed = [], $forSubscription = false) {
    $username = $this->config->get('envoke_api_id');
    $password = $this->config->get('envoke_api_key');

    if ($forSubscription == true) {
      $username = $this->config->get('envoke_subscription_api_id');
      $password = $this->config->get('envoke_subscription_api_key');
    }

    $envoke_contact_api_url = 'https://e1.envoke.com/v1/contacts';

    $contact = [
      "email" => $email,
      "consent_status" => "Express",
      "consent_description" => "Express consent given on website homepage form."
    ];

    $get_contact_response = $this->httpClient->get($envoke_contact_api_url, [
      'auth' => [$username, $password],
      'query' => ['filter[email]' => $email],
      'http_errors' => false
    ]);

    $sleepTime = 0;
    while ($get_contact_response->getStatusCode() == 401) {
      $sleepTime += self::SLEEP_INCREMENT;
      if ($sleepTime > self::SLEEP_LIMIT) {
        break;
      }
      usleep($sleepTime);

      $get_contact_response = $this->httpClient->get($envoke_contact_api_url, [
        'auth' => [$username, $password],
        'query' => ['filter[email]' => $email],
        'http_errors' => false
      ]);
    }

    if ($get_contact_response->getStatusCode() == 200) {
      $contacts = json_decode($get_contact_response->getBody());
      if (is_array($contacts) && count($contacts) > 0) {
        if ($forSubscription == true) {
          $new_contact_data = [];
          $existing_contact = $contacts[0];
          if (!empty($subscribed) || !empty($unsubscribed)) {
            // update subscriptions
            $current_subscriptions = $existing_contact->interests;
            foreach ($current_subscriptions as $key) {
              $new_contact_data["interests"][$key] = "Set";
            }
            foreach ($subscribed as $key) {
              $new_contact_data["interests"][$key] = "Set";
            }
            foreach ($unsubscribed as $key) {
              $new_contact_data["interests"][$key] = "Unset";
            }

            // switch to Express to allow resubscribe
            if (count($subscribed) > 0) {
              $new_contact_data['consent_status'] = 'Express';
            }
            // update user
            $existing_contact_id = $existing_contact->id;
            $update_contact_response = $this->httpClient->patch($envoke_contact_api_url . "/{$existing_contact_id}", [
              'auth' => [$username, $password],
              'json' => $new_contact_data
            ]);
            $result = json_decode($update_contact_response->getBody(), true);
            return $result['success'];
          }
        } else {
          return TRUE;
        }
      }
    }
    else {
      // TODO: Add drupal watchdog entry.
    }

    try {
      if ($forSubscription) {
        foreach ($subscribed as $key) {
          $contact["interests"][$key] = "Set";
        }
        foreach ($unsubscribed as $key) {
          $contact["interests"][$key] = "Unset";
        }
      }
      $insert_contact_response = $this->httpClient->post($envoke_contact_api_url, [
        'auth' => [$username, $password],
        'json' => $contact,
      ]);
      $result = json_decode($insert_contact_response->getBody(), true);
      return $result['success'];
    } catch (RequestException $e) {
      return FALSE;
    }
  }

  public function getContactSubscriptions($email) {
    $username = $this->config->get('envoke_subscription_api_id');
    $password = $this->config->get('envoke_subscription_api_key');

    $envoke_contact_api_url = 'https://e1.envoke.com/v1/contacts';

    $get_contact_response = $this->httpClient->get($envoke_contact_api_url, [
      'auth' => [$username, $password],
      'query' => ['filter[email]' => $email],
      'http_errors' => false
    ]);

    $contact_subscriptions = [];

    if ($get_contact_response->getStatusCode() == 200) {
      $contacts = json_decode($get_contact_response->getBody());
      if (is_array($contacts) && count($contacts) > 0) {
        $existing_contact = $contacts[0];
        if ($existing_contact->consent_status !== 'Revoked') {
          $contact_subscriptions = $existing_contact->interests;
        }
      }
    }

    return $contact_subscriptions;
  }
}
